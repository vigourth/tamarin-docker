{ pkgs ? import <nixpkgs> { } }:
let
  port = "3001";
in
pkgs.dockerTools.buildImage
{
  name = "tamarin";
  tag = "latest";
  created = "now";
  copyToRoot = pkgs.buildEnv {
    name = "root";
    paths = [
      pkgs.tamarin-prover
      pkgs.bash
      ./content
    ];
    pathsToLink = [ "/bin" "/" ];
  };

  runAsRoot = ''
    mkdir -p /tmp/
  '';
  config = {
    ExposedPorts = {
      "${port}" = { };
    };
    Entrypoint = [ "${pkgs.tamarin-prover}/bin/tamarin-prover" "interactive" "--port=${port}" "--interface=*4" "--auto-sources" ];
    Cmd = [ "/" ];
    WorkingDir = "/";
  };
}
