# Image docker Tamarin créée avec Nix

## Utilisation du docker

L'image est mise à jour automatiquement en CI, donc la dernière
version est toujours accessible en faisant:
```bash
docker run --pull=always gricad-registry.univ-grenoble-alpes.fr/vigourth/tamarin-docker:latest
```

Dans un navigateur, ouvrir [http://172.17.0.2:3001/](http://172.17.0.2:3001/)

Ce docker lance toujours `tamarin` en mode interactif, avec les bons
paramètres d'initialisation. Par défaut `tamarin` est lancé sans
argument pour la résolution, mais il est possible de les ajouter en
faisant:

```bash
# Tamarin "equivalence"  mode:
docker run ... --diff /

# Tamarin interactive mode that allows to generate (when it is  possible) automatically the "sources lemma":
docker run ... --auto-sources /
```

__ATTENTION LE `/` EST IMPORTANT__

## Utilisation par `nix`

Si vous avez `nix` vous pouvez ne pas vous servir de ce dépot et
simplement faire:
```bash
nix-shell -p tamarin-prover --run "tamarin-prover interactive --interface=*4 --port=3001 content/"
```

Puis ouvrir [http://localhost:3001/](http://localhost:3001/)

## Utilisation avancée

Si les arguments pour tamarin ne sont pas les bons, on peut utiliser
l'option `--entrypoint` de docker.

## Création de l'image

_Il faut avoir `nix` d'installé._

Pour créer l'image docker et la charger:
```bash
docker load <$(nix-build)
```

